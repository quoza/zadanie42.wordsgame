import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj imie gracza nr 1.");
        String playerName1 = sc.next();
        System.out.println("Podaj imie gracza nr 2.");
        String playerName2 = sc.next();
        Player p1 = new Player(playerName1, "/home/kuba/Projekty/Zadanie41.WordsGame/player1.txt");
        Player p2 = new Player(playerName2, "/home/kuba/Projekty/Zadanie41.WordsGame/player2.txt");
        Random rand = new Random();
        int startingWordNumber = rand.nextInt(p1.getWordsList().size());
        String word = p1.getWordsList().get(startingWordNumber);
        char lastChar = GameUtil.getLastCharOfWord(word);
        GameUtil.removeWordFromPlayersList(word, p1);
        int playerNumber = 2;
        boolean isOver = false;

        do {
            if (playerNumber == 1) {
                if (GameUtil.doesPlayerHaveNextWord(lastChar, p1)) {
                    String nextWord = GameUtil.playerHaveNextWord(lastChar, p1);
                    GameUtil.removeWordFromPlayersList(nextWord, p1);
                    lastChar = GameUtil.getLastCharOfWord(nextWord);
                    playerNumber = 2;
                } else{
                    System.out.println("Player 1 has lost the game.");
                    isOver = true;
                }
            }

            if (playerNumber == 2) {
                if (GameUtil.doesPlayerHaveNextWord(lastChar, p2)) {
                    String nextWord = GameUtil.playerHaveNextWord(lastChar, p2);
                    GameUtil.removeWordFromPlayersList(nextWord, p2);
                    lastChar = GameUtil.getLastCharOfWord(nextWord);
                    playerNumber = 1;
                } else {
                    System.out.println("Player 2 has lost the game.");
                    isOver = true;
                }
            } else {
                System.out.println("Player does not exist.");
            }
        } while (!isOver);
    }
}