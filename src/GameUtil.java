public class GameUtil {

    public GameUtil() {
    }

    public static char getLastCharOfWord(String word){
        return word.charAt(word.length());
    }

    public static boolean doesPlayerHaveNextWord(char firstChar, Player player){
        for (String word : player.getWordsList()){
            if(word.charAt(0) == firstChar){
                return true;
            }
        }
        return false;
    }

    public static String playerHaveNextWord(char firstChar, Player player){
        for (String word : player.getWordsList()){
            if(word.charAt(0) == firstChar){
                System.out.println(player.getPlayerName() + ": " + word);
                return word;
            }
        }
        return "";
    }

    public static void removeWordFromPlayersList(String word, Player player){
        player.getWordsList().remove(word);
    }
}
