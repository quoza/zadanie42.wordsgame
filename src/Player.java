import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Player {
    private List<String> wordsList = new ArrayList<>();
    private String playerName;

    private void fillList(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner reader = new Scanner(file);
        while(reader.hasNext()){
            wordsList.add(reader.next());
        }
        reader.close();
    }

    public Player(String playerName, String filePath) throws FileNotFoundException {
        fillList(filePath);
        this.playerName = playerName;
    }

    public List<String> getWordsList() {
        return wordsList;
    }

    public void setWordsList(List<String> wordsList) {
        this.wordsList = wordsList;
    }

    public String getPlayerName() {
        return playerName;
    }
}
